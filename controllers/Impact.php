<?php namespace LTN\ElearningCourses\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Impact extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'ltn.ynternet.elearning.manage_courses', 
        'ltn.ynternet.elearning.manage_content' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('LTN.ElearningCourses', 'Ynternet eLearning project', 'side-menu-item');
    }
}
