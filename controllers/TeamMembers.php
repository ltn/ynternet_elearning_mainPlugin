<?php namespace LTN\ElearningCourses\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

class TeamMembers extends Controller
{
    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var array
     */
    public $implement = ['Backend\Behaviors\ListController', 'Backend\Behaviors\FormController', 'Backend\Behaviors\ReorderController'];

    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';

    /**
     * @var string
     */
    public $reorderConfig = 'config_reorder.yaml';

    /**
     * @var array
     */
    public $requiredPermissions = [
        'ltn.ynternet.elearning.manage_content'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('LTN.ElearningCourses', 'team', 'team-members');
    }
}
