<?php return [
    'plugin' => [
        'name' => 'eLearning Courses',
        'description' => 'This plugin will hold all courses information for the Ynternet eLearning school project',
        'menu_name' => 'Ynternet eLearning project',
        'sort' => 'Réorganiser',
    ],
    'course' => [
        'title' => 'Title',
        'slug' => 'Slug',
        'full_description' => 'Full description',
        'small_description' => 'Small description',
        'manage_courses' => 'Manage courses',
        'manage_menu' => 'Cours',
        'plural_label' => 'Cours',
        'singular_label' => 'Cours',
        'tags' => 'Tags',
        'number_of_module' => 'Nombre de modules',
        'picture' => 'Header',
    ],
    'module' => [
        'title' => 'Title',
        'intro' => 'Introduction',
        'slug' => 'Slug',
        'plural_label' => 'Modules',
        'manage_menu' => 'Modules',
        'position' => 'Position du cours dans la liste',
        'content' => 'Content',
        'conclusion' => 'Conclusion',
    ],
    'modules' => [
        'manage_menu' => 'Modules',
        'picture' => 'Picture',
        'youtube_id' => 'Youtube ID',
        'conferences' => 'Conferences',
        'course' => 'Cours',
    ],
    'tag' => [
        'label' => 'Label',
        'parent' => 'Parent',
        'title' => 'Tags',
    ],
    'conference' => [
        'title' => 'Titre',
        'tab1' => 'Info',
        'date' => 'Date',
        'geo' => 'Geo',
        'address' => 'Adresse',
        'zipcode' => 'Zip Code',
        'city' => 'City',
        'country' => 'Country',
        'description' => 'Description',
        'link' => 'Link',
        'menu' => 'Conferences',
        'slug' => 'Slug',
        'modules' => 'Modules',
        'small_description' => 'Short description',
        'picture' => 'Header',
        'intervenant' => 'Intervenant',
        'isOnHome' => 'Sur la home',
    ],
    'blog' => [
        'title' => 'Title',
        'slug' => 'Slug',
        'content' => 'Content',
        'date' => 'Date',
        'menu' => 'Blog',
    ],
    'pageContent' => [
        'label' => 'Label',
        'Content' => 'Contenu',
        'menu' => 'Contents',
        'title' => 'Titre',
        'slug' => 'slug',
        'in_menu' => 'Dans le menu',
        'order' => 'Ordre si dans le menu',
        'color' => 'Couleur (si dans le menu)',
    ],
    'teamCategory' => [
        'label' => 'Label',
        'order' => 'Ordre',
        'mainMenu' => 'Équipe',
    ],
    'teamMember' => [
        'name' => 'Nom',
        'picture' => 'Photo',
        'description' => 'Description',
        'link' => 'Lien',
        'category' => 'Catégorie',
    ],
    'FondationContent' => [
        'id' => 'ID',
        'title' => 'Titre',
        'link' => 'URL',
        'text' => 'Texte',
        'updatedAt' => 'Modifié le',
        'menu' => 'Fondation',
        'picture' => 'Image',
    ],
];
