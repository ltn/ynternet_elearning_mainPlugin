<?php namespace LTN\ElearningCourses\Models;

use Model;

/**
 * Model
 */
class TeamMember extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array
     */
    public $belongsTo = [
        'category' => [
            '\LTN\ElearningCourses\Models\TeamCategory',
            'key'      => 'category_id',
            'otherKey' => 'id',
            'order'    => 'order asc'
        ]
    ];

    /**
     * Softly implement the TranslatableModel behavior.
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /*
     * Validation
     */
    /**
     * @var array
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ltn_elearningcourses_team_members';

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    /**
     * @var mixed
     */
    public $timestamps = false;

    /**
     * @var array
     */
    public $translatable = ['name', 'description'];
}
