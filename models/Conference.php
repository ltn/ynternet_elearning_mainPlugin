<?php namespace LTN\ElearningCourses\Models;

use Model;

/**
 * Model
 */
class Conference extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \LTN\ElearningCourses\Traits\Taggable;

    /**
     * Softly implement the TranslatableModel behavior.
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    /**
     * @var array Attributes that support translation, if available.
     */
    public $translatable = ['title', 'description', 'small_description'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ltn_elearningcourses_conferences';

    public $belongsToMany = [
        'modules' => [
            '\LTN\ElearningCourses\Models\Module',
            'table' => 'ltn_elearningcourses_conference_module',
            'key'      => 'conference_id',
            'otherKey' => 'module_id'
        ]
    ];

    public $belongsTo = [
        'team_member' => [
            '\LTN\ElearningCourses\Models\TeamMember',
            'key'      => 'team_member_id',
            'otherKey' => 'id'
        ]
    ];

	/**
     * Scope a query to only include conference which should be on the home
     */
    public function scopeIsOnHome($query)
    {
        return $query->where('is_on_home', '=', 1);
    }
}
