<?php namespace LTN\ElearningCourses\Models;

use Model;

/**
 * Model
 */
class FondationContent extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ltn_elearningcourses_fondation_content';
}