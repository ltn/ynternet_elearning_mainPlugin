<?php namespace LTN\ElearningCourses\Models;

use Model;

/**
 * Model
 */
class ProjectCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array
     */
    public $hasMany = [
        'projets' => [
            '\LTN\ElearningCourses\Models\Project',
            'key'      => 'category_id',
            'otherKey' => 'id',
            'order'    => 'order asc'
        ]
    ];

    /**
     * Softly implement the TranslatableModel behavior.
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /*
     * Validation
     */
    /**
     * @var array
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ltn_elearningcourses_project_categories';

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    /**
     * @var mixed
     */
    public $timestamps = false;

    /**
     * @var array
     */
    public $translatable = ['label'];
}
