<?php namespace LTN\ElearningCourses\Models;

use Model;

/**
 * Model
 */
class Impact extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ltn_elearningcourses_impact';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
