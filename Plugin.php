<?php namespace LTN\ElearningCourses;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    // public function registerComponents()
    // {
    //     // return [
    //     //     'Owl\FormWidgets\Tagbox\Widget' => [
    //     //         'label' => 'Tagbox',
    //     //         'code'  => 'owl-tagbox',
    //     //         'path' => 'test'
    //     //     ],
    //     // ];
    // }

    // public function registerSettings()
    // {
    // }

    public function boot() {
        date_default_timezone_set('Europe/Paris');
        setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');
    }

    /*
     * Owl Registration
     *
     * @return  array
     */
    public function registerFormWidgets()
    {
        $formWidgets = array(
            'LTN\ElearningCourses\FormWidgets\Tagbox' => [
                'label' => 'Tagbox',
                'code'  => 'tagbox'
            ],
        );
        return $formWidgets;
    }
}
