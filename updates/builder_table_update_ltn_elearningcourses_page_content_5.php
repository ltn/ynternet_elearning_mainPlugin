<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesPageContent5 extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_page_content', function($table)
        {
            $table->increments('id')->change();
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_page_content', function($table)
        {
            $table->integer('id')->change();
        });
    }
}
