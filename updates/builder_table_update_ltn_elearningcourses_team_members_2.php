<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesTeamMembers2 extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_team_members', function($table)
        {
            $table->integer('order')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_team_members', function($table)
        {
            $table->dropColumn('order');
        });
    }
}
