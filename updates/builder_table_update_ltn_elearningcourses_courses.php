<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesCourses extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_courses', function($table)
        {
            $table->string('picture')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_courses', function($table)
        {
            $table->dropColumn('picture');
        });
    }
}
