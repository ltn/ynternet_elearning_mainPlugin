<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesConferences4 extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_conferences', function($table)
        {
            $table->dropColumn('modules');
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_conferences', function($table)
        {
            $table->string('modules', 255)->nullable();
        });
    }
}
