<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesPageContent6 extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_page_content', function($table)
        {
            $table->boolean('in_menu')->default(0);
            $table->integer('order')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_page_content', function($table)
        {
            $table->dropColumn('in_menu');
            $table->dropColumn('order');
        });
    }
}
