<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesConferenceModule2 extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_conference_module', function($table)
        {
            $table->renameColumn('modules_id', 'module_id');
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_conference_module', function($table)
        {
            $table->renameColumn('module_id', 'modules_id');
        });
    }
}
