<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLtnElearningcoursesTeamMembers extends Migration
{
    public function up()
    {
        Schema::create('ltn_elearningcourses_team_members', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name')->nullable();
            $table->string('picture')->nullable();
            $table->text('description')->nullable();
            $table->string('link')->nullable();
            $table->integer('category')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ltn_elearningcourses_team_members');
    }
}
