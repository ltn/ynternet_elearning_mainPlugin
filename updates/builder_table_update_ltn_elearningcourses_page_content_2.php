<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesPageContent2 extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_page_content', function($table)
        {
            $table->text('content')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_page_content', function($table)
        {
            $table->text('content')->nullable(false)->change();
        });
    }
}
