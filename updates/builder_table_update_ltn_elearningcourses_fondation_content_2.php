<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesFondationContent2 extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_fondation_content', function($table)
        {
            $table->text('picture')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_fondation_content', function($table)
        {
            $table->dropColumn('picture');
        });
    }
}
