<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLtnElearningcoursesImpact extends Migration
{
    public function up()
    {
        Schema::create('ltn_elearningcourses_impact', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('activity')->nullable();
            $table->string('title')->nullable();
            $table->string('project')->nullable();
            $table->string('main_partner')->nullable();
            $table->integer('number_of_participants')->nullable();
            $table->integer('hours_of_training')->nullable();
            $table->date('date')->nullable();
            $table->text('evaluation')->nullable();
            $table->text('documentation')->nullable();
            $table->string('month')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ltn_elearningcourses_impact');
    }
}
