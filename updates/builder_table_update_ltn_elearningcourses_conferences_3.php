<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesConferences3 extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_conferences', function($table)
        {
            $table->string('small_description')->nullable();
            $table->string('picture')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_conferences', function($table)
        {
            $table->dropColumn('small_description');
            $table->dropColumn('picture');
        });
    }
}
