<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesConferenceModule extends Migration
{
    public function up()
    {
        Schema::rename('ltn_elearningcourses_conference_modules', 'ltn_elearningcourses_conference_module');
    }
    
    public function down()
    {
        Schema::rename('ltn_elearningcourses_conference_module', 'ltn_elearningcourses_conference_modules');
    }
}
