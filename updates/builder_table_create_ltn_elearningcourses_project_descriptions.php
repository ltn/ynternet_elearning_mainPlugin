<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLtnElearningcoursesProjectDescriptions extends Migration
{
    public function up()
    {
        Schema::create('ltn_elearningcourses_project_descriptions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('picture')->nullable();
            $table->text('description')->nullable();
            $table->integer('order')->default(0);
            $table->string('title')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ltn_elearningcourses_project_descriptions');
    }
}
