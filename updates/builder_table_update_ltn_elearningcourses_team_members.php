<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesTeamMembers extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_team_members', function($table)
        {
            $table->renameColumn('category', 'category_id');
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_team_members', function($table)
        {
            $table->renameColumn('category_id', 'category');
        });
    }
}
