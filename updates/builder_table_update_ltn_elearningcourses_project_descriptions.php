<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesProjectDescriptions extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_project_descriptions', function($table)
        {
            $table->integer('category_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_project_descriptions', function($table)
        {
            $table->dropColumn('category_id');
        });
    }
}
