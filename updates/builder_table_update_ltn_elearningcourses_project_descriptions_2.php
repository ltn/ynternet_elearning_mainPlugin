<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesProjectDescriptions2 extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_project_descriptions', function($table)
        {
            $table->integer('order')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_project_descriptions', function($table)
        {
            $table->integer('order')->nullable(false)->change();
        });
    }
}
