<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLtnElearningcoursesConferenceModules extends Migration
{
    public function up()
    {
        Schema::create('ltn_elearningcourses_conference_modules', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('conference_id')->unsigned();
            $table->integer('modules_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ltn_elearningcourses_conference_modules');
    }
}
