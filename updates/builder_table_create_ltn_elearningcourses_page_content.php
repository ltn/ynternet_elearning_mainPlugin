<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLtnElearningcoursesPageContent extends Migration
{
    public function up()
    {
        Schema::create('ltn_elearningcourses_page_content', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('label');
            $table->text('content');
            $table->primary(['label']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ltn_elearningcourses_page_content');
    }
}
