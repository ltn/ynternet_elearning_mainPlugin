<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesPageContent extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_page_content', function($table)
        {
            $table->dropPrimary(['label']);
            $table->integer('id')->unsigned();
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_page_content', function($table)
        {
            $table->dropPrimary(['id']);
            $table->dropColumn('id');
            $table->primary(['label']);
        });
    }
}
