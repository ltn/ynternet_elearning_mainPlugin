<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesPageContent7 extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_page_content', function($table)
        {
            $table->string('color')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_page_content', function($table)
        {
            $table->dropColumn('color');
        });
    }
}
