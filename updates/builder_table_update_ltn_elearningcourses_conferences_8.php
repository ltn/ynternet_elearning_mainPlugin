<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLtnElearningcoursesConferences8 extends Migration
{
    public function up()
    {
        Schema::table('ltn_elearningcourses_conferences', function($table)
        {
            $table->boolean('is_on_home')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ltn_elearningcourses_conferences', function($table)
        {
            $table->boolean('is_on_home')->default(1)->change();
        });
    }
}
