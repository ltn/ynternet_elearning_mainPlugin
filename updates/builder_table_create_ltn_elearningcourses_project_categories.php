<?php namespace LTN\ElearningCourses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLtnElearningcoursesProjectCategories extends Migration
{
    public function up()
    {
        Schema::create('ltn_elearningcourses_project_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('label')->nullable();
            $table->integer('order')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ltn_elearningcourses_project_categories');
    }
}
